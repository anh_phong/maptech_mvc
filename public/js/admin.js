$(".img-upload").on("click",function (){
    $(".img-upload").removeClass("selected");
    $(this).toggleClass("selected");
})
$("#file-img").on("change", function (){
    var img = $('#file-img').prop('files')[0]; console.log(img);
    var form_data = new FormData();
    //thêm files vào trong form data
    form_data.append('img', img);
    $.ajax({
        url: '/ajax/upload', // gửi đến file upload.php
        dataType: 'text',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        success: function (res) {
            if (res.indexOf('/') != -1){
                toastr.success("Tải lên thành công!");
                $("input[name=img-current]").val(img.name);
                $("#img-current").attr('src','http://'+res);
            }
            else {
                toastr.error(res);
            }
        }
    });
})
$(".img-upload").on("click", function (){
    var img = $(this).attr('data-img');
    $("input[name=img-current]").val(img);
    $("#img-current").attr('src','/public/img/'+img);
})
$("#edit-img").on("submit",function (e){
    e.preventDefault();
    var data = {
        id:$("input[name=img-id]").val(),
        img:$("input[name=img-current]").val(),
        type_img:$("select[name=type-img]").val(),
        type_news:$("select[name=type-news]").val(),
    }
    $.ajax({
        url: "/ajax/saveImg",
        type: 'POST',
        data: data,
        dataType: 'json',
        //contentType: 'application/json',
        success: function(msg) {
            if (!msg.status){
                toastr.error(msg.messages);
            }
            else toastr.success(msg.messages);
        }
    });
})
$(".delete-img").on("click",function (){
    var element = $(this);
    var data = {
        id : $(this).attr("data-id")
    }
    console.log(1);

    $.ajax({
        url: "/ajax/deleteImg",
        type: 'POST',
        data: data,
        dataType: 'json',
        //contentType: 'application/json',
        success: function(msg) {
            console.log(msg);
            if (!msg.status){
                toastr.error(msg.messages);
            }
            else {
                element.closest("tr").hide();
                toastr.success(msg.messages);
            }
        }
    });
})
$(".topic_edit").on("submit",function (e){
    e.preventDefault();
    var formData = new FormData($(this)[0]);
    // for(var pair of formData.entries()) {
    //     console.log(pair[0]+ ', '+ pair[1]);
    // }
    $.ajax({
        url: "/ajax/topicSave",
        type: 'POST',
        data: formData,
        dataType: 'json',
        processData: false,
        contentType: false,
        //contentType: 'application/json',
        success: function(msg) {
            if (!msg.status){
                toastr.error(msg.messages);
            }
            else {
                location.reload();
            }
        }
    });
})
$(".delete-topic").on("click",function (){
    var element = $(this);
    var data = {
        id : $(this).attr("data-id")
    }
    $.ajax({
        url: "/ajax/topicDelete",
        type: 'POST',
        data: data,
        dataType: 'json',
        //contentType: 'application/json',
        success: function(msg) {
            console.log(msg);
            if (!msg.status){
                toastr.error(msg.messages);
            }
            else {
                element.closest("tr").hide();
                toastr.success(msg.messages);
            }
        }
    });
})
$('.summernote').summernote({
    height:500,
    callbacks: {
        onImageUpload: function(files, editor, welEditable) {
            sendFile(files[0], editor, welEditable);
        }
    }
});
function sendFile(file, editor, welEditable) {
    data = new FormData();
    data.append("img", file);
    $.ajax({
        data: data,
        type: "POST",
        url: "/ajax/upload",
        cache: false,
        contentType: false,
        processData: false,
        success: function(url) {
            if (url.indexOf('/') != -1){
                var image = $('<img>').attr('src', 'http://' + url);
                $('.summernote').summernote("insertNode", image[0]);
            }
            else {
                toastr.error(url);
            }
        }
    });
}
$("#news_save").on("submit",function (e){
    e.preventDefault();
    var formData = new FormData($(this)[0]);
    formData.append('content',$(".summernote").summernote('code'));
    for(var pair of formData.entries()) {
        console.log(pair[0]+ ', '+ pair[1]);
    }
    $.ajax({
        url: "/ajax/newsSave",
        type: 'POST',
        data: formData,
        dataType: 'json',
        processData: false,
        contentType: false,
        //contentType: 'application/json',
        success: function(msg) {
            if (!msg.status){
                toastr.error(msg.messages);
            }
            else {
                toastr.success(msg.messages);
            }
        }
    });
})
$(".delete-news").on("click",function (){
    var element = $(this);
    var data = {
        id : $(this).attr("data-id")
    }
    $.ajax({
        url: "/ajax/newsDelete",
        type: 'POST',
        data: data,
        dataType: 'json',
        //contentType: 'application/json',
        success: function(msg) {
            console.log(msg);
            if (!msg.status){
                toastr.error(msg.messages);
            }
            else {
                element.closest("tr").hide();
                toastr.success(msg.messages);
            }
        }
    });
})
$("#change_pass").on("submit",function (e){
    e.preventDefault();
    var formData = new FormData($(this)[0]);
    $.ajax({
        url: "/ajax/changePass",
        type: 'POST',
        data: formData,
        dataType: 'json',
        processData: false,
        contentType: false,
        //contentType: 'application/json',
        success: function(msg) {
            if (!msg.status){
                toastr.error(msg.messages);
            }
            else {
                toastr.success(msg.messages);
            }
        }
    });
})