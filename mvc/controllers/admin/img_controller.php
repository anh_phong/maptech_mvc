<?php
class img_controller extends controler
{
    public function img()
    {
        $data = $this->model('img');
        $join =[
            'JOIN img_topic ON img.id_topic = img_topic.id',
            'LEFT JOIN news ON img.id_news = news.id'
        ];
        $order= "order by img.id desc";
        $field = [
            'img.id','img.img','img.create_at','img_topic.topic','news.title'
        ];
        $img = json_decode($data->getJoin($join,$w=null,$l=null,$field,$order),true);
        $this->view('admin',[
            'page'=>'img/list',
            'img'=>$img
        ]);
    }
    public function imgSave($id = null)
    {
        $img = $this->model('img');
        $data_img = json_decode($img->get(["id = '$id'"]),true);
        $topic = $this->model('img_topic');
        $img_topic = json_decode($topic->get(),true);
        $news = $this->model('news');
        $data_news = json_decode($news->get(),true);
        $directory = "public/img";
        $img_upload = glob($directory . "/*");
        if (empty($id)){
            $this->view('admin',[
                'page'=>'img/edit',
                'img_topic'=>$img_topic,
                'news'=>$data_news,
                'img_upload'=>$img_upload
            ]);
        }
        else{
            $this->view('admin',[
                'page'=>'img/edit',
                'img'=>$data_img[0],
                'img_topic'=>$img_topic,
                'news'=>$data_news,
                'img_upload'=>$img_upload
            ]);
        }

    }
}


?>

