<?php
require_once "./mvc/controllers/admin/img_controller.php";
class topic_controller extends img_controller
{
    public function topic()
    {
        $topic = $this->model('img_topic');
        $data = json_decode($topic->get(),true);
        $this->view('admin',[
            'page'=>'img_topic/list',
            'topic'=>$data
        ]);
    }
}


?>