<?php
require_once "./mvc/controllers/admin/topic_controller.php";
class news_controller extends topic_controller
{
    public function news()
    {
        $js = [
            '/public/assets/demo/default/custom/header/actions.js'
        ];
        $news = $this->model('news');
        $data = json_decode($news->get(),true);
        $this->view('admin',[
            'page'=>'news/list',
            'news'=>$data,
            'js'=>$js
        ]);
    }
    public function newsSave($id = null)
    {
        $news = $this->model('news');
        if (empty($id)){
            $this->view('admin',[
                'page'=>'news/edit'
            ]);
        }
        else{
            $this->view('admin',[
                'page'=>'news/edit',
                'news'=>json_decode($news->get(["id = '$id'"]),true)[0],
            ]);
        }

    }
}


?>