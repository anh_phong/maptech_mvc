<?php
require_once "./mvc/controllers/admin/news_controller.php";

class Admin extends news_controller
{
    public function __construct()
    {
        if (!isset($_SESSION["user"])) {
            header("location:./login");
            exit();
        }
    }
    public function myhome()
    {
        $css = [
            '/public/assets/vendors/custom/fullcalendar/fullcalendar.bundle.css'
        ];
        $js = [
            '/public/assets/vendors/custom/fullcalendar/fullcalendar.bundle.js',
            '/public/assets/app/js/dashboard.js'
        ];
        $this->view('admin',[
            'page'=>'dashboard/dashboard',
            'css'=>$css,
            'js'=>$js
        ]);
    }
    public function changePass()
    {
        $this->view('admin',[
            'page'=>'dashboard/change_pass',
        ]);
    }
   
}

?>