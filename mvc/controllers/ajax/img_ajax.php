<?php
class img_ajax extends Controler
{
    public function upload()
    {
        if(isset($_POST) && !empty($_FILES['img'])) {
            $target_dir = file;
            $target_file = $target_dir . basename($_FILES["img"]["name"]);
            $uploadOk = 1;
            $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
        if ($uploadOk == 1 && file_exists($target_file)) {
            echo $_SERVER['SERVER_NAME'].':'.$_SERVER['SERVER_PORT'].'/public/img/'.$_FILES['img']['name'];
            $uploadOk = 0;
          }
          // Check file size
          if ($uploadOk == 1 && $_FILES["img"]["size"] > 5000000) {
              echo 'File quá lớn!.';
              $uploadOk = 0;
          }
          
          // Allow certain file formats
          if($uploadOk == 1 && $imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
          && $imageFileType != "gif") {
            echo('File không đúng định dạng!.');
              $uploadOk = 0;
          }

            if ($uploadOk == 1 && move_uploaded_file($_FILES["img"]["tmp_name"], $target_file)) {
                echo $_SERVER['SERVER_NAME'].':'.$_SERVER['SERVER_PORT'].'/public/img/'.$_FILES['img']['name'];
            }
        }
    }
    public function saveImg()
    {
        $id = $_POST['id'];
        $img_content = $_POST['img'];
        $type_img = $_POST['type_img'];
        $type_news = $_POST['type_news'];

        $img = $this->model('img');
        $data = [
            'img'=>"$img_content",
            'id_topic'=>"$type_img",
            'id_news'=>"$type_news",
            'create_at'=>time()
        ];
        if (!empty($id)){
            $where = ["id = '$id'"];
        }

        else $where = null;
        if ($img->save($data,$where)){
            $res = [
                'status'=>1,
                'messages'=>'ok'
            ];
            echo json_encode($res);
        }
        else {
            $res = [
                'status'=>0,
                'messages'=>'False!'
            ];
            echo json_encode($res);
        }
    }
    public function deleteImg()
    {

        $id = $_POST['id'];
        $img = $this->model('img');
        $where = ["id = '$id'"];
        if ($img->delete($where)){
            $res = [
                'status'=>1,
                'messages'=>'ok'
            ];
            echo json_encode($res);
        }
        else {
            $res = [
                'status'=>0,
                'messages'=>'False!'
            ];
            echo json_encode($res);
        }
    }
}

?>
