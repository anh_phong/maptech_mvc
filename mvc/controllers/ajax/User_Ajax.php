<?php
require_once "./mvc/controllers/ajax/news_ajax.php";
class user_ajax extends news_ajax
{
    public function checkUserLogin()
    {
        $id = $_POST['id'];
        $pass = sha1($_POST['pass']);
        $users = $this->model("users");
        $notifi = json_decode($users->get(["username = '$id'","password = '$pass'"]),true)[0];
        $response = [
            "status"=>0,
            "messsage"=>"Đăng nhập không thành công!"
        ];
        if($notifi){
            $_SESSION['user'] = $notifi['username'];
            $response = [
                "status"=>1,
                "message"=>"OK"
            ];
        }
        echo json_encode($response);
    }
    public function saveUser($id = false)
    {
        $username = $_POST['id'];
        $pass = sha1($_POST['pass']);
        $re_pass = sha1($_POST['repass']);
        $email = $_POST['email'];
        $w = empty($id)? null : ["id= '$id'"]; 
        $data = [
            'username'=>$username,
            'password'=>$pass,
            'email'=>$email,
            'create_at'=>time()
        ];
        if ($pass != $re_pass) {
            $response = [
                "status"=>0,
                "messages"=>"Xác nhận mật khẩu không đúng!"
            ];
        }
        else{
            $users = $this->model("users");
            $status = $users->save($data,$w);
            if ($status) {
                $_SESSION['user'] = $username;
                $response = [
                        "status"=>1,
                        "messages"=>"ok!"
                    ];
            }
            else {
                $response = [
                        "status"=>0,
                        "messages"=>"Tài khoản đã tồn tại!"
                    ];
            }
        }
        echo json_encode($response);
    }
    public function forgotPassword()
    {
        $username = $_POST['id'];
        $email = $_POST['email'];
        $users = $this->model("users");
        $notifi = json_decode($users->get(["username = '$username'","email = '$email'"]),true);
        if($notifi){
            $_SESSION['otp']['code'] = random_int(111111,999999);
            $_SESSION['otp']['expired'] = 60;
            $_SESSION['otp']['start'] = time();
            $_SESSION['otp']['id'] = $notifi[0]['id'];
            $_SESSION['otp']['username'] = $notifi[0]['username'];
            if($this->sendMail($_SESSION['otp']['code'],$email)){
                $response = [
                    'status'=>1,
                    'messages'=>'Nhập OTP chúng tôi đã gửi về mail của bạn!'
                ];
            }
            else $response = [
                'status'=>0,
                'messages'=>'Gửi mail thất bại!'
            ];
        }
        else $response = [
            'status'=>0,
            'messages'=>'Tài khoản không tồn tại!'
        ];
        echo json_encode($response);
    }
    public function authOTP()
    {
        $otp = $_POST['otp'];
        if($otp){
            if ($_SESSION['otp']['code'] == $otp && time()-$_SESSION['otp']['start'] < $_SESSION['otp']['expired']) {
                $response = [
                    'status'=>1,
                    'messages'=>'Xác thực thành công!'
                ];
            }
            else $response = [
                'status'=>0,
                'messages'=>'Mã OTP sai hoặc quá hạn!'
            ];
        }
        else $response = [
            'status'=>0,
            'messages'=>'Mã OTP không tồn tại!'
        ];
        echo json_encode($response);
    }
    public function resetPass()
    {
        $id = $_SESSION['otp']['id'];
        $pass = sha1($_POST['pass_new']);
        $data = [
            'password'=>$pass,
        ];
        $w=[
            "id = $id"
        ];
       
        $users = $this->model("users");
        $status = $users->save($data,$w);
        if ($status) {
            $_SESSION['user'] = $_SESSION['otp']['username'];
            $response = [
                    "status"=>1,
                    "messages"=>"ok!"
                ];
        }
        else {
            $response = [
                    "status"=>0,
                    "messages"=>"False!"
                ];
        }
        unset($_SESSION['otp']);
        echo json_encode($response);
    }
    public function changePass()
    {
        $username = $_SESSION['user'];
        $pass_old = sha1($_POST['pass']);
        $pass_new = sha1($_POST['pass_new']);
        $re_pass_new = sha1($_POST['re_pass_new']);
        if($pass_new != $re_pass_new){
            die(json_encode([
                'status'=>0,
                'messages'=>'Xác nhận mật khẩu không đúng!'
            ]));
        }
        $users = $this->model("users");
        $user = json_decode($users->get(["username = '$username'"]),true);
        if($pass_old != $user[0]['password']){
            die(json_encode([
                'status'=>0,
                'messages'=>'Mật khẩu cũ không đúng!'
            ]));
        }
        $data = [
            'password'=>$pass_new,
        ];
        $w=[
            "username = '$username'"
        ];
        $status = $users->save($data,$w);
        if ($status) {
            $response = [
                    "status"=>1,
                    "messages"=>"ok!"
                ];
        }
        else {
            $response = [
                    "status"=>0,
                    "messages"=>"False!"
                ];
        }
        echo json_encode($response);
    }
}


?>