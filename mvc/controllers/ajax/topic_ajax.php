<?php
require_once "./mvc/controllers/ajax/img_ajax.php";
class topic_ajax extends img_ajax
{
    public function topicSave()
    {
        $id = 0;
        if (isset($_POST['topic_id'])){
            $id = $_POST['topic_id'];
        }
        $name = $_POST['topic_name'];
        $topic = $this->model('img_topic');
        $data = [
            'topic'=>"$name",
            'create_at'=>time()
        ];
        if (!empty($id)){
            $where = ["id = '$id'"];
        }

        else $where = null;
        if ($topic->save($data,$where)){
            $res = [
                'status'=>1,
                'messages'=>'ok'
            ];
            echo json_encode($res);
        }
        else {
            $res = [
                'status'=>0,
                'messages'=>'False!'
            ];
            echo json_encode($res);
        }
    }
    public function topicDelete()
    {
        $id = $_POST['id'];
        $img = $this->model('img_topic');
        $where = ["id = '$id'"];
        if ($img->delete($where)){
            $res = [
                'status'=>1,
                'messages'=>'ok'
            ];
            echo json_encode($res);
        }
        else {
            $res = [
                'status'=>0,
                'messages'=>'False!'
            ];
            echo json_encode($res);
        }
    }
}


?>
