<?php
require_once "./mvc/controllers/ajax/topic_ajax.php";
class news_ajax extends topic_ajax
{
    public function newsSave()
    {
        
        $id = empty($_POST['news-id'])? 0 : $_POST['news-id'];
        $title = $_POST['title'];
        $content = $_POST['content'];
        $news = $this->model('news');
        $data = !empty($id)? [
            'title'=>$title,
            'content'=>$content
            ] 
            : [
                'title'=>$title,
                'content'=>$content,
                'create_at'=>time()
            ];
        $where = !empty($id)? ["id ='$id'"] : [];
        
        if ($news->save($data,$where)){
            $res = [
                'status'=>1,
                'messages'=>'ok'
            ];
            echo json_encode($res);
        }
        else {
            $res = [
                'status'=>0,
                'messages'=>'False!'
            ];
            echo json_encode($res);
        }
    }
    public function newsDelete()
    {
        $id = $_POST['id'];
        $news = $this->model('news');
        $where = ["id = '$id'"];
        if ($news->delete($where)){
            $res = [
                'status'=>1,
                'messages'=>'ok'
            ];
            echo json_encode($res);
        }
        else {
            $res = [
                'status'=>0,
                'messages'=>'False!'
            ];
            echo json_encode($res);
        }
    }
}
?>
