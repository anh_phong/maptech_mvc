<?php

class DB
{
    public $con;
    public function __construct()
    {
        if(!defined('map')) die('Hack lam cho');
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $host = "localhost";
        $uname = "root";
        $pass = "";
        $db = "maptect";

        $this->con = mysqli_connect($host, $uname, $pass) or die ("could not connect to mysql"); 
        mysqli_select_db($this->con, $db) or die("no database");
        mysqli_query($this->con, "SET NAMES 'utf8'");

        
    }
    //select
    public function getDefineData($sql,$order)
    {
        $sql .= $order; //echo  $sql;die;
        $resutl = $this->con->query($sql);
        $data = array();
        if ($resutl&&$resutl->num_rows>0) {
            while ($row = $resutl->fetch_assoc()) {
                $data[] = $row;
            }
            return json_encode($data);
        }
    }
    // get field,where,top là mảng 1 chiều
    public function getData($field = [], $table, $where = [], $top = [],$order)
    {
        $sql = "SELECT ".implode(",",$field)."
                FROM $table ";
        if (!empty($where)) {
            $sql .= "WHERE ".implode(" AND ",$where);
        }
        if (!empty($top)) {
            $sql .= " LIMIT ".implode(",",$top);
        }
        return $this->getDefineData($sql,$order);
    }
    //join join là mảng 1 chiều
    public function getJoinData($field = [], $table, $join = [], $where = [], $top = [],$order)
    {
        $table .= " ".implode(" ",$join);
        return $this->getData($field, $table, $where, $top,$order);
    }
    //insert data là mảng 2 chiều
    public function insertData($table,$data = [])
    {
        $field = "";
        $values = "";
        foreach ($data as $key => $value) {
            $field .= ", $key";
            $values .= ", '$value'";
        }
        $sql = "INSERT INTO $table(".trim($field,',').") VALUE (".trim($values,',').")"; //echo $sql; die;
        if ($this->con->query($sql)) {
            return true;
        }
        else return false;
    }
    //update data 2 chieu và where là mảng 1 chiều
    public function updateData($table, $data = [], $where = [])
    {
        $sql = "UPDATE $table SET ";
        if (!empty($data)) {
            foreach ($data as $key => $value){
                $sql .= "$key = '$value',";
            }
            $sql = trim($sql,",");
        }
        if (!empty($where)) {
            $sql .= " WHERE ".implode(" AND ",$where);
        }// echo $sql; die;
        if ($this->con->query($sql)) {
            return true;
        }
        else return false;
    }
    //delete where là mảng 1 chiều
    public function delData($table, $where = [])
    {
        $sql = "DELETE FROM $table ";
        if (!empty($where)) {
            $sql .= "WHERE ".implode(" AND ",$where);
        }
        if ($this->con->query($sql)) {
            return true;
        }
        else return false;
    }
}


?>