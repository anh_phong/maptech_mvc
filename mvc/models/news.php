<?php
class news extends DB
{
    public $table = "news";
    //field,where,limit là mảng 1 chiều
    public function get($where = [],$limit = null,$field = ["*"],$order=null)
    {
        $data = json_decode($this->getData($field,$this->table,$where,$limit,$order),true);
        foreach ($data as $key => $value){
            $arr[$key] = $value;
            $arr[$key]['content'] = html_entity_decode(htmlspecialchars_decode($value['content']));
        }
        return json_encode($arr);
    }
    //data là mảng 2 chiều, where là mảng 1 chiều
    //create or update
    public function save($data = [],$where = [])
    {
        if (empty($data)) return false;
        if (isset($data['content'])){
            $data['content'] = htmlentities(htmlspecialchars($data['content']));
        }
        if (empty($where)){
            return $this->insertData($this->table,$data);
        }
        return $this->updateData($this->table,$data,$where);
    }
    //del where mang 1 chieu
    public function delete($where = [])
    {
        if (empty($where)) return false;
        return $this->delData($this->table,$where);
    }
}


?>