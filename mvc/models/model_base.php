<?php
class model_base extends DB
{
    public $table;
    //field,where,limit là mảng 1 chiều
    public function get($where = [],$limit = null,$field = ["*"],$order=null)
    {
        return $this->getData($field,$this->table,$where,$limit,$order);
    }
    //join,field,where,limit là mảng 1 chiều
    public function getJoin($join = [],$where = [],$limit = null,$field = ["*"],$order=null)
    {
        return $this->getJoinData($field,$this->table,$join,$where,$limit,$order);
    }
    //data là mảng 2 chiều, where là mảng 1 chiều
    //create or update
    public function save($data = [],$where = [])
    {
        if (empty($data)) return false;
        if (empty($where)){
            return $this->insertData($this->table,$data);
        }
        return $this->updateData($this->table,$data,$where);
    }
    //del where mang 1 chieu
    public function delete($where = [])
    {
        if (empty($where)) return false;
        return $this->delData($this->table,$where);
    }
}


?>