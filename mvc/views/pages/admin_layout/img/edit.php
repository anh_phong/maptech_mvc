<?php //var_dump($data['img']['id']); die; 
?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Hình Ảnh
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="<?php echo $host . 'admin' ?>" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="<?php echo $host . 'admin/news' ?>" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Bài Viết Cho Dịch Vụ
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">

                <!--begin::Portlet-->
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-title" style="border-right: 1px solid #e2e5ec;
	                            padding: 7px 25px 7px 0; margin: 0 15px 0 0;">
                                    Cập Nhật Hình Ảnh
                                </h3>
                                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                                    <li class="m-nav__item m-nav__item--home">
                                        <a href="<?php echo $host . 'admin/img' ?>" class="m-nav__link m-nav__link--icon">
                                            <i class="m-nav__link-icon la la-home"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <a href="<?php echo $host . 'admin/img' ?>" class="btn btn-info m-btn m-btn--custom m-btn--icon m-btn--air">
                                        <span>
                                            <i class="la la-arrow-left"></i>
                                            <span>
                                                Quay lại
                                            </span>
                                        </span>
                                    </a>
                                </li>
                                <li class="m-portlet__nav-item"></li>
                            </ul>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <form id="edit-img" class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="" method="post" enctype="multipart/form-data">
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group row">
                                <label class="col-lg-2 col-form-label">
                                    Hình ảnh:
                                </label>
                                <div class="col-lg-4">
                                    <input type="text" name="img-current" class="form-control m-input" value="<?php echo isset($data['img']['img']) ? $data['img']['img'] : ''; ?>" readonly>
                                </div>
                                <label class="col-lg-2 col-form-label">
                                    Hình ảnh đã chọn:
                                </label>
                                <div class="col-lg-4">
                                    <img id="img-current" src="<?php echo isset($data['img']['img']) ? $host . "public/img/" . $data['img']['img'] : '' ?>" alt="" style="max-width: 100%;">
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-2 col-form-label">
                                    Loại:
                                </label>
                                <div class="col-lg-4">
                                    <select name="type-img" class="form-control m-input">
                                        <option value="0">---</option>
                                        <?php
                                        foreach ($data['img_topic'] as $key => $value) { ?>
                                            <option value="<?php echo $value['id'] ?>" <?php if (isset($data['img'])) {
                                                                                            echo $value['id'] == $data['img']['id_topic'] ? 'selected' : '';
                                                                                        } ?>>
                                                <?php echo $value['topic'] ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <label class="col-lg-2 col-form-label">
                                    Thuộc bài viết:
                                </label>
                                <div class="col-lg-4">
                                    <select name="type-news" class="form-control m-input">
                                        <option value="0">---</option>
                                        <?php
                                        foreach ($data['news'] as $key => $value) { ?>
                                            <option value="<?php echo $value['id'] ?>" <?php if (isset($data['img'])) {
                                                                                            echo $value['id'] == $data['img']['id_news'] ? 'selected' : '';
                                                                                        } ?>>
                                                <?php echo $value['title'] ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-2 col-form-label">
                                    Tải lên:
                                </label>
                                <div class="col-lg-4">
                                    <input type="file" class="form-control" name="file-img" id="file-img">
                                </div>
                                <label class="col-lg-2 col-form-label">
                                    Hoặc chọn ảnh đã có:
                                </label>
                                <div class="col-lg-4">
                                    <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#m_modal_4">
                                        Chọn Ảnh
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                            <div class="m-form__actions m-form__actions--solid">
                                <div class="row">
                                    <div class="col-lg-5"></div>
                                    <div class="col-lg-7">
                                        <input type='hidden' name='img-id' value='<?php echo isset($data['img']['id']) ? $data['img']['id'] : ''; ?>'>
                                        <button type="submit" class="btn btn-brand">
                                            Lưu
                                        </button>
                                        <button type="reset" class="btn btn-secondary">
                                            Hủy
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>
        </div>
    </div>
</div>
<!--begin::Modal-->
<div class="modal fade" id="m_modal_4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    Danh sách ảnh đã tải lên
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" data-height="500">

                    <div class="form-group d-flex flex-wrap">
                        <?php foreach ($data['img_upload'] as $value) { ?>
                            <div class="img-upload" style="width: 20%;" data-img="<?php echo basename($value) ?>">
                                <img src="/<?php echo $value ?>" alt="" style="max-width: 100%;">
                            </div>
                        <?php } ?>
                    </div>


                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    Đóng
                </button>
            </div>
        </div>
    </div>
</div>
<!--end::Modal-->