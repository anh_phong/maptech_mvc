<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Topic
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="<?php echo $host.'admin' ?>" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="<?php echo $host.'admin/news' ?>" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Bài Viết Cho Dịch Vụ
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Danh sách danh mục ảnh
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a data-toggle="modal" data-target="#topic_modal_add" class="btn btn-info m-btn m-btn--custom m-btn--icon m-btn--air">
                                <span>
                                    <i class="la la-plus"></i>
                                    <span>
                                        Thêm danh mục
                                    </span>
                                </span>
                            </a>
                        </li>
                        <li class="m-portlet__nav-item"></li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">
                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                    <thead>
                        <tr>
                            <th>
                                ID
                            </th>
                            <th>
                                Topic
                            </th>
                            <th>
                                Created
                            </th>
                            <th>
                                Option
                            </th>
                        </tr>
                    </thead>
                    <tbody>
					<?php foreach ($data['topic'] as $key => $value){ ?>
					<tr>
                        <td>
                            <?php echo $value['id'] ?>
                        </td>
                        <td>
                            <?php echo $value['topic'] ?>
                        </td>
                        <td>
                            <?php echo date('d-m-Y H:i:s',$value['create_at']) ?>
                        </td>
                        <td nowrap>
							<span class="dropdown">
								<a href="javacsript:;" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="true">
								<i class="la la-ellipsis-h"></i>
								</a>
								<div class="dropdown-menu dropdown-menu-right">
									<a class="dropdown-item" data-toggle="modal" data-target="#topic_modal<?php echo $value['id'] ?>"><i class="la la-edit"></i> Sửa</a>
									<a class="delete-topic dropdown-item" data-id="<?php echo $value['id'] ?>"><i class="la la-eraser"></i> Xóa</a>
								</div>
							</span>
                        </td>
					</tr>
                        <!-- MODAL-->
                        <div class="modal fade" id="topic_modal<?php echo $value['id'] ?>" tabindex="-1" role="dialog"
                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-xl" role="document">
                                <form class="topic_edit">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">
                                                Sửa Topic
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">
                                                    &times;
                                                </span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true"
                                                 data-height="100">
                                                <div class="form-group d-flex flex-wrap">
                                                    <input type="text" name="topic_name" class="form-control" value="<?php echo $value['topic'] ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                Đóng
                                            </button>
                                            <input type="hidden" name="topic_id" value="<?php echo $value['id'] ?>">
                                            <button type="submit" class="btn btn-primary">
                                                Lưu
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- END MODAL-->
                <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
<!-- MODAL-->
<div class="modal fade" id="topic_modal_add" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <form class="topic_edit">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        Thêm Topic
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            &times;
                        </span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true"
                         data-height="100">
                        <div class="form-group d-flex flex-wrap">
                            <input type="text" name="topic_name" class="form-control" value="">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Đóng
                    </button>
                    <button type="submit" class="btn btn-primary">
                        Lưu
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- END MODAL-->
