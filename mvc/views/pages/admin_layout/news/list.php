<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Bài viết các dịch vụ
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="<?php echo $host.'admin' ?>" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="<?php echo $host.'admin/news' ?>" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Hình ảnh
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Danh sách bài viết
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="<?php echo $host.'admin/newsSave' ?>" class="btn btn-info m-btn m-btn--custom m-btn--icon m-btn--air">
                                <span>
                                    <i class="la la-plus"></i>
                                    <span>
                                        Thêm bài viết
                                    </span>
                                </span>
                            </a>
                        </li>
                        <li class="m-portlet__nav-item"></li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">
                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                    <thead>
                        <tr>
                            <th>
                                ID
                            </th>
                            <th>
                                Title
                            </th>
                            <th>
                                Content
                            </th>
                            <th>
                                Created
                            </th>
                            <th>
                                Option
                            </th>
                        </tr>
                    </thead>
                    <tbody>
					<?php foreach ($data['news'] as $key => $value){ ?>
					<tr>
                        <td>
                            <?php echo $value['id'] ?>
                        </td>
                        <td>
                                <?php echo $value['title'] ?>
                        </td>
                        <td>
                                <?php echo $value['content'] ?>
                        </td>
                        <td>
                                <?php echo date('d-m-Y H:i:s',$value['create_at']) ?>
                        </td>
                        <td nowrap>
							<span class="dropdown">
								<a href="javacsript:;" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="true">
								<i class="la la-ellipsis-h"></i>
								</a>
								<div class="dropdown-menu dropdown-menu-right">
									<a class="dropdown-item" href="<?php echo $host."admin/newsSave/".$value['id'] ?>"><i class="la la-edit"></i> Sửa</a>
									<a class="delete-news dropdown-item" data-id="<?php echo $value['id'] ?>"><i class="la la-eraser"></i> Xóa</a>
								</div>
							</span>	
                        </td>
					</tr>
                <?php } ?> 
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
