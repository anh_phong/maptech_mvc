<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Đổi mật khẩu
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="<?php echo $host . 'admin' ?>" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="<?php echo $host . 'admin/news' ?>" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Bài Viết Cho Dịch Vụ
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">

                <!--begin::Portlet-->
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-title" style="border-right: 1px solid #e2e5ec;
	                            padding: 7px 25px 7px 0; margin: 0 15px 0 0;">
                                    Đổi mật khẩu
                                </h3>
                                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                                    <li class="m-nav__item m-nav__item--home">
                                        <a href="<?php echo $host . 'admin/img' ?>" class="m-nav__link m-nav__link--icon">
                                            <i class="m-nav__link-icon la la-home"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <a href="<?php echo $host . 'admin/img' ?>" class="btn btn-info m-btn m-btn--custom m-btn--icon m-btn--air">
                                        <span>
                                            <i class="la la-arrow-left"></i>
                                            <span>
                                                Quay lại
                                            </span>
                                        </span>
                                    </a>
                                </li>
                                <li class="m-portlet__nav-item"></li>
                            </ul>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <form id="change_pass" class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="" method="post" enctype="multipart/form-data">
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 col-form-label">
                                    Mật khẩu cũ:
                                </label>
                                <div class="col-lg-7">
                                    <input type="password" name="pass" class="form-control m-input">
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 col-form-label">
                                    Mật khẩu mới:
                                </label>
                                <div class="col-lg-7">
                                    <input type="password" name="pass_new" class="form-control m-input">
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 col-form-label">
                                    Xác nhận mật khẩu mới:
                                </label>
                                <div class="col-lg-7">
                                    <input type="password" class="form-control" name="re_pass_new">
                                </div>
                            </div>
                        </div>

                        <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                            <div class="m-form__actions m-form__actions--solid">
                                <div class="row">
                                    <div class="col-lg-5"></div>
                                    <div class="col-lg-7">
                                        <button type="submit" class="btn btn-brand">
                                            Lưu
                                        </button>
                                        <button type="reset" class="btn btn-secondary">
                                            Hủy
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>
        </div>
    </div>
</div>
