<?php
//$logo = [];
//foreach ($data['img'] as $key => $value){
//    if ($value['tag']=="logo"){
//        $logo[] = $value;
//    }
//}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="public/css/asset/bootstrap.4.5.0.min.css">
    <link rel="stylesheet" href="public/css/asset/slick.css">
    <link rel="stylesheet" type="text/css" href="public/css/home.css">
    <link rel="shortcut icon" href="./public/img/<?php echo $data['logo']['img'] ?>">
    <script src="https://kit.fontawesome.com/373c99e1e8.js" crossorigin="anonymous"></script>
    <title>Map Tech</title>
</head>
<body style="font-weight: 600">
<div class="container-fluid" style="margin-bottom:0">
    <div class="row header">
        <div class="col-4">
            <div class="hotline">
                <p><i class="fa fas-phone"></i> Hotline</p>
                <h3>0123456789</h3>
            </div>

        </div>
        <div class="col-4"></div>
        <div class="col-4 logo">
            <img src="./public/img/<?php echo $data['logo']['img'] ?>" alt="">
        </div>
    </div>
    <div class="row">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark" style="width: 100%">

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">MapTech <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#intro">Giới Thiệu</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#service">Dịch Vụ</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#contact">Hợp Tác</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    <div class="row" id="slide">
        <div class=" col slide">
            <div class="item">
                <img src="./public/img/banner1.jpg" alt="" class="img-fluid">
            </div>
            <div class="item">
                <img src="./public/img/banner1.jpg" alt="" class="img-fluid">
            </div>
            <div class="item">
                <img src="./public/img/banner1.jpg" alt="" class="img-fluid">
            </div>
        </div>
        <div class="info">
            <div class="info-work">
                <h3>Giờ làm việc</h3>
                <ul>
                    <li>
                        <div class="time-work">
                            <div class=""><i class="far fa-clock"></i><span>Thứ 2- Thứ 6 </span></div>
                            <div class=""><span>8:00 AM - 5:00 PM</span></div>
                        </div>
                        <hr>
                    </li>
                    <li>
                        <div class="time-work">
                            <div class=""><i class="far fa-clock"></i><span>Thứ 2- Thứ 6 </span></div>
                            <div class=""><span>8:00 AM - 5:00 PM</span></div>
                        </div>
                        <hr>
                    </li>
                    <li>
                        <div class="time-work">
                            <div class=""><i class="far fa-clock"></i><span>Thứ 2- Thứ 6 </span></div>
                            <div class=""><span>8:00 AM - 5:00 PM</span></div>
                        </div>
                        <hr>
                    </li>
                </ul>
            </div>
            <div class="info-work">
                <h3>Chi tiết liên hệ</h3>
                <ul>
                    <li>
                        <div class="time-work">
                            <div class=""><i class="fas fa-map-marker-alt"></i><span> Địa chỉ: xx đường xx </span></div>
                        </div>
                        <hr>
                        <button class="map-button">Xem bản đồ</button>
                    </li>
                    <li>
                        <div class="time-work">
                            <div class="">

                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="info-work">
                <h3>Giờ làm việc</h3>
                <ul>
                    <li>
                        <div class="time-work">
                            <div class=""><i class="far fa-clock"></i><span>Thứ 2- Thứ 6 </span></div>
                            <div class=""><span>8:00 AM - 5:00 PM</span></div>
                        </div>
                        <hr>
                    </li>
                    <li>
                        <div class="time-work">
                            <div class=""><i class="far fa-clock"></i><span>Thứ 2- Thứ 6 </span></div>
                            <div class=""><span>8:00 AM - 5:00 PM</span></div>
                        </div>
                        <hr>
                    </li>
                    <li>
                        <div class="time-work">
                            <div class=""><i class="far fa-clock"></i><span>Thứ 2- Thứ 6 </span></div>
                            <div class=""><span>8:00 AM - 5:00 PM</span></div>
                        </div>
                        <hr>
                    </li>
                </ul>
            </div>

        </div>
    </div>
    <div class="row service" id="service">
        <div class="col">
            <img id="service-game" src="./public/img/digital-content.png" class="img-fluid">
            <p>
                Phát triển và sản xuất trò chơi trực tuyến, phát hành ra thị trường quốc tế. Nhập khẩu và Phát hành các trò chơi nổi tiếng thế giới.
            </p>
            <button>Xem thêm...</button>
        </div>
        <div class="col">
            <h2>Dịch vụ</h2>
            <div class="description">
                MAP TECH tập trung vào 4 nhóm sản phẩm chủ lực, mang đến cho người dùng những trải nghiệm phong phú và đơn giản hơn.
            </div>
            <ul>
                <?php
                foreach ($data['news'] as $key=>$value){?>
                    <li>
                        <a href="#news<?php echo $key ?>"><?php echo $value['title'] ?></a>
                    </li>
                <?php }
                ?>

                <li>
                    <a href="#service2">Thiết kế ứng dụng Web</a>
                </li>
                <li>
                    <a href="#service3">Thiết kế ứng dụng Web</a>
                </li>
                <li>
                    <a href="#service4">Thiết kế ứng dụng Web</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="row" id="intro">
        <div class="col intro">
            <h2>Chúng tôi là MAP TECH</h2>
            <p>Thành lập ngày 9/9/2020, MAP TECH hiện là công ty Internet và công nghệ hàng đầu, kỳ lân công nghệ 1 tỷ USD + duy nhất của Việt Nam.</p>
        </div>
    </div>
    <div class="row">
        <div class="col slide-intro">
            <div class="">
                <img width="400" height="400" src="./public/img/intro.jpg" alt="" class="img-fluid">
            </div>
            <div class="">
                <img width="400" height="400" src="./public/img/intro.jpg" alt="" class="img-fluid">
            </div>
            <div class="">
                <img width="400" height="400" src="./public/img/intro.jpg" alt="" class="img-fluid">
            </div>
            <div class="">
                <img width="400" height="400" src="./public/img/intro.jpg" alt="" class="img-fluid">
            </div>
            <div class="">
                <img width="400" height="400" src="./public/img/intro.jpg" alt="" class="img-fluid">
            </div>
        </div>
    </div>
    <div class="row road-map">
        <div class="col">
            <div class="road-map-title">
                <h2>ROADMAP</h2>
            </div>
            <ul class="map">
                <li class="items">
                    <img class="icon" src="./public/img/1.png" alt="">
                    <span class="line-h"></span>
                    <hr>
                    <span class="point"></span>

                    <div class="items-title">
                        <h4>Khởi nghiệp</h4>
                    </div>
                    <div class="date">9/2020</div>
                </li>
                <span class="line-w"></span><li class="items">
                    <img src="./public/img/1.png" alt="">
                    <span class="line-h"></span>
                    <hr>
                    <span class="point"></span>
                    <div class="items-title">
                        <h4>Khởi nghiệp</h4>
                    </div>
                    <div class="date">9/2020</div>
                </li>
                <span class="line-w"></span><li class="items">
                    <img src="./public/img/1.png" alt="">
                    <span class="line-h"></span>
                    <hr>
                    <span class="point"></span>
                    <div class="items-title">
                        <h4>Khởi nghiệp</h4>
                    </div>
                    <div class="date">9/2020</div>
                </li>
                <span class="line-w"></span><li class="items">
                    <img src="./public/img/1.png" alt="">
                    <span class="line-h"></span>
                    <hr>
                    <span class="point"></span>
                    <div class="items-title">
                        <h4>Khởi nghiệp</h4>
                    </div>
                    <div class="date">9/2020</div>
                </li>
                <span class="line-w"></span><li class="items">
                    <img src="./public/img/1.png" alt="">
                    <span class="line-h"></span>
                    <span class="point"></span>
                    <div class="items-title">
                        <h4>Khởi nghiệp</h4>
                    </div>
                    <div class="date">9/2020</div>
                </li>
                <span class="line-w"></span>
            </ul>
            <div class="roadmap-description">
                Mở đường cho kỷ nguyên game nhập vai tại Việt Nam với thành công kỷ lục của Võ Lâm Truyền Kỳ: 20.000 PCU (lượng người chơi truy cập tại cùng một thời điểm).
            </div>
        </div>
    </div>
    <div class="row product">
        <div class="col">
            <?php
            foreach ($data['news'] as $key=>$value){ ?>
                <div class="product-service" id="news<?php echo $key ?>">
                    <div class="p-title">
                        <h2><?php echo $value['title'] ?></h2>
                    </div>
                    <div class="p-content">
                        <p><?php echo $value['content'] ?></p>
                    </div>
                    <div class="product-item">
                        <ul>
                            <li>
                                <img src="./public/img/product.png" alt="" class="img-fluid">
                                <div class="product-item-info">
                                    <p>sản phẩm 1</p>
                                    <div class="product-price">500 $</div>
                                </div>
                            </li>
                            <li>
                                <img src="./public/img/product.png" alt="" class="img-fluid">
                                <div class="product-item-info">
                                    <p>sản phẩm 1</p>
                                    <div class="product-price">500 $</div>
                                </div>
                            </li>
                            <li>
                                <img src="./public/img/product.png" alt="" class="img-fluid">
                                <div class="product-item-info">
                                    <p>sản phẩm 1</p>
                                    <div class="product-price">500 $</div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            <?php    }
            ?>

            <div class="product-service" id="service2">
                <div class="p-title">
                    <h2>Dịch vụ thiết kế website trọn gói</h2>
                </div>
                <div class="p-content">
                    <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                        Numquam reiciendis tempora ducimus accusamus minus nostrum,
                        quisquam ea eveniet quaerat suscipit debitis aliquam,
                        odit totam nulla voluptatibus in a et quo!
                    </p>
                </div>
                <div class="product-item">
                    <ul>
                        <li>
                            <img src="./public/img/product.png" alt="" class="img-fluid">
                            <div class="product-item-info">
                                <p>sản phẩm 1</p>
                                <div class="product-price">500 $</div>
                            </div>
                        </li>
                        <li>
                            <img src="./public/img/product.png" alt="" class="img-fluid">
                            <div class="product-item-info">
                                <p>sản phẩm 1</p>
                                <div class="product-price">500 $</div>
                            </div>
                        </li>
                        <li>
                            <img src="./public/img/product.png" alt="" class="img-fluid">
                            <div class="product-item-info">
                                <p>sản phẩm 1</p>
                                <div class="product-price">500 $</div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div><div class="product-service" id="service3">
                <div class="p-title">
                    <h2>Dịch vụ thiết kế website trọn gói</h2>
                </div>
                <div class="p-content">
                    <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                        Numquam reiciendis tempora ducimus accusamus minus nostrum,
                        quisquam ea eveniet quaerat suscipit debitis aliquam,
                        odit totam nulla voluptatibus in a et quo!
                    </p>
                </div>
                <div class="product-item">
                    <ul>
                        <li>
                            <img src="./public/img/product.png" alt="" class="img-fluid">
                            <div class="product-item-info">
                                <p>sản phẩm 1</p>
                                <div class="product-price">500 $</div>
                            </div>
                        </li>
                        <li>
                            <img src="./public/img/product.png" alt="" class="img-fluid">
                            <div class="product-item-info">
                                <p>sản phẩm 1</p>
                                <div class="product-price">500 $</div>
                            </div>
                        </li>
                        <li>
                            <img src="./public/img/product.png" alt="" class="img-fluid">
                            <div class="product-item-info">
                                <p>sản phẩm 1</p>
                                <div class="product-price">500 $</div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div><div class="product-service" id="service4">
                <div class="p-title">
                    <h2>Dịch vụ thiết kế website trọn gói</h2>
                </div>
                <div class="p-content">
                    <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                        Numquam reiciendis tempora ducimus accusamus minus nostrum,
                        quisquam ea eveniet quaerat suscipit debitis aliquam,
                        odit totam nulla voluptatibus in a et quo!
                    </p>
                </div>
                <div class="product-item">
                    <ul>
                        <li>
                            <img src="./public/img/product.png" alt="" class="img-fluid">
                            <div class="product-item-info">
                                <p>sản phẩm 1</p>
                                <div class="product-price">500 $</div>
                            </div>
                        </li>
                        <li>
                            <img src="./public/img/product.png" alt="" class="img-fluid">
                            <div class="product-item-info">
                                <p>sản phẩm 1</p>
                                <div class="product-price">500 $</div>
                            </div>
                        </li>
                        <li>
                            <img src="./public/img/product.png" alt="" class="img-fluid">
                            <div class="product-item-info">
                                <p>sản phẩm 1</p>
                                <div class="product-price">500 $</div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
    <div class="row contact" id="contact">
        <div class="col-4">
            <img src="./public/img/contact.jpg" alt="" class="img-fluid">
        </div>
        <div class="col-6">
            <h2>Liên Hệ Hợp Tác</h2>
            <h5>tiêu đề</h5>
            <p>giới thiệu..</p>
            <p>Sunt in culpa qui officia deserunt mollit anim id est laborum consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>
        </div>
    </div>

</div>
<div class="footer">
    <p>Coppyright@Maptech</p>
    <p>43 Phạm Văn Đồng - Từ Liêm - Hà Nội</p>
</div>
<script src="public/js/asset/jquery-3.5.1.min.js"></script>
<script src="public/js/asset/bootstrap.4.5.0.min.js"></script>
<script src="public/js/asset/popper.1.16.0.min.js"></script>
<script src="public/js/asset/slick.js"></script>
<script src="public/js/home.js"></script>
<script>
    $(document).ready(function(){
        $(".slide").slick({
            arrows: false,
            dots: true,
            autoplay: true,
            autoplaySpeed: 3000,
        });
        $('.slide-intro').slick({
            centerMode: true,
            centerPadding: '60px',
            slidesToShow: 4,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 1
                    }
                }
            ],
            arrows: false,
            autoplay: true,
            autoplaySpeed:2000
        });
        // $(".tabs li").on("click", function(){
        //   var i = $(this).index();
        //   $(".tabs li").removeClass("on");
        //   $(this).addClass("on");
        //   $(".tabs-content").css({"display":"none"});
        //   $(".tabs-content").eq(i).css({"display":"block"});
        // })
        // var sectbg = [
        //     "url(./public/images/kiemkhach.jpg) no-repeat",
        //     "url(./public/images/phapsubg.jpg) no-repeat",
        //     "url(./public/images/vusu.jpg) no-repeat",
        //     "url(./public/images/kiemkhach.jpg) no-repeat",
        //     "url(./public/images/phapsubg.jpg) no-repeat",
        //     "url(./public/images/vusu.jpg) no-repeat",
        //     "url(./public/images/kiemkhach.jpg) no-repeat",
        //     "url(./public/images/phapsubg.jpg) no-repeat",
        //     "url(./public/images/vusu.jpg) no-repeat",
        //     "url(./public/images/kiemkhach.jpg) no-repeat",
        //     "url(./public/images/phapsubg.jpg) no-repeat",
        //     "url(./public/images/vusu.jpg) no-repeat",
        // ];
        // $(".sect-items").mouseover(function(){
        //   var i = $(this).index();
        //   $(".sect-bg").css({"background": sectbg[i],"background-size":"contain"});
        // });
    });
</script>
</body>
</html>