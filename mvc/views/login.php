
<html lang="en" >
	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		<title>
			Đăng Nhập || Đăng Ký
		</title>
		<meta name="description" content="Latest updates and statistic charts">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
		<!--begin::Web font -->
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
          WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
		</script>
		<!--end::Web font -->
        <!--begin::Base Styles -->

		<link href="./public/css/asset/vendors.bundle.css" rel="stylesheet" type="text/css" />
		<link href="./public/css/asset/style.bundle.css" rel="stylesheet" type="text/css" />
        
		<!--end::Base Styles -->
		<link rel="shortcut icon" href="./public/img/logo.png" />
		
		<link href="https://fonts.googleapis.com/css?family=Varela+Round&display=swap" rel="stylesheet">
		<style type="text/css">
			.font-vn{
				font-family: 'Varela Round', sans-serif !important;
			}
			.alert-display{
				display: none;
				position: absolute;
				right: 0;
			}
		</style>
	</head>
	<!-- end::Head -->
    <!-- end::Body -->
	<body  class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default font-vn">
		<!-- begin:: Page -->
		<div class="m-grid m-grid--hor m-grid--root m-page">
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--signin m-login--2 m-login-2--skin-1" id="m_login" style="background: url(./public/assets/app/media/img/bg/bg-1.jpg),#7861f5; position: relative;">
				<div class="m-grid__item m-grid__item--fluid m-login__wrapper">
					<div class="m-login__container">
						<div class="m-login__logo">
							<a href="#">
								<img src="<?php echo $host.'public/img/'.$data['logo']['img'] ?>">
							</a>
						</div>
						<div class="m-login__signin">
							<div class="m-login__head">
								<h3 class="m-login__title">
									Đăng Nhập
								</h3>
							</div>
							<form id="formlogin" class="m-login__form m-form" action=""  method="post">
								<div class="form-group m-form__group">
									<input class="form-control m-input" id = "id"   type="text" placeholder="Tên đăng nhập" name="id" autocomplete="off">
								</div>
								<div class="form-group m-form__group">
									<input class="form-control m-input m-login__form-input--last" id = "pass" type="password" placeholder="Mật khẩu" name="password">
								</div>
								
								<div class="row m-login__form-sub">
									<div class="col m--align-left m-login__form-left">
										<label class="m-checkbox  m-checkbox--light">
											<input type="checkbox" name="remember" value="1">
											Nhớ
											<span></span>
										</label>
									</div>
									<div class="col m--align-right m-login__form-right">
										<a href="javascript:;" id="m_login_forget_password" class="m-link">
											Quên Mật Khẩu ?
										</a>
									</div>

								</div>
								<div class="m-login__form-action">
									<input type="submit" name="submit" value="Đăng Nhập" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn m-login__btn--primary font-vn">
									
									<a href="./home" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn m-login__btn--primary font-vn">
										Trang Chủ
									</a>
									
								</div>
							</form>
						</div>
						<div class="m-login__signup">
							<div class="m-login__head">
								<h3 class="m-login__title">
									Đăng Ký
								</h3>
								<div class="m-login__desc">
									Vui lòng điền đầy đủ thông tin:
								</div>
							</div>
							<form id="form_register" class="m-login__form m-form" action="" method="post">
								<div class="form-group m-form__group">
									<input class="form-control m-input" type="text" placeholder="Tên đăng nhập" name="id">
								</div>								
								<div class="form-group m-form__group">
									<input class="form-control m-input" type="password" placeholder="Mật khẩu" name="pass">
								</div>
								<div class="form-group m-form__group">
									<input class="form-control m-input m-login__form-input--last" type="password" placeholder="Xác nhận mật khẩu" name="repass">
								</div>
								<div class="form-group m-form__group">
									<input class="form-control m-input" type="email" placeholder="Email" name="email">
								</div>								
								<div class="m-login__form-action">
									<input type="submit" name="reg" value="Đăng ký" class="btn m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn m-login__btn--primary font-vn">
									&nbsp;&nbsp;
									<button id="m_login_signup_cancel" class="btn m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn font-vn">
										Hủy
									</button>
								</div>
							</form>
						</div>
						<div class="m-login__forget-password">
							<div class="m-login__head">
								<h3 class="m-login__title">
									Quên mật khẩu ?
								</h3>
								<div class="m-login__desc">
									Nhập số điện thoại đăng ký và tên đăng nhập để lấy lại mật khẩu:
								</div>
							</div>
							<form id="forgot" class="m-login__form m-form" action="" method="post">
								<div class="form-group m-form__group">
									<input class="form-control m-input" type="text" placeholder="Tên đăng nhập" name="id">
								</div>
								<div class="form-group m-form__group">
									<input class="form-control m-input" type="email" placeholder="Email đã đăng ký cho tài khoản này" name="email">
								</div>
								<div class="m-login__form-action">
									<button type="submit" name="forgot" class="btn m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn m-login__btn--primary font-vn">
										Yêu cầu
									</button>
									&nbsp;&nbsp;
									<button id="m_login_forget_password_cancel" class="btn m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn font-vn">
										Hủy
									</button>
								</div>
							</form>
						</div>
						<div class="m-login__account">
							<span class="m-login__account-msg">
								Chưa có tài khoản ?
							</span>
							&nbsp;&nbsp;
							<a href="javascript:;" id="m_login_signup" class="m-link m-link--light m-login__account-link">
								Đăng ký ngay
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- end:: Page -->
    	<!--begin::Base Scripts -->
        <script src="./public/js/jquery-3.4.1.min.js"></script>
		<script src="./public/js/asset/vendors.bundle.js" type="text/javascript"></script>
		<script src="./public/js/asset/scripts.bundle.js" type="text/javascript"></script>
		<!--end::Base Scripts -->   
        <!--begin::Page Snippets -->
		<script src="./public/js/asset/login.js" type="text/javascript"></script>
		<script src="./public/js/asset/toastr.js" type="text/javascript"></script>

		<script>
			toastr.options = {
				  "closeButton": false,
				  "debug": false,
				  "newestOnTop": false,
				  "progressBar": false,
				  "positionClass": "toast-top-right",
				  "preventDuplicates": false,
				  "onclick": null,
				  "showDuration": "300",
				  "hideDuration": "1000",
				  "timeOut": "5000",
				  "extendedTimeOut": "1000",
				  "showEasing": "swing",
				  "hideEasing": "linear",
				  "showMethod": "fadeIn",
				  "hideMethod": "fadeOut"
				};

				
		</script>
		
		<script>
			$(document).ready(function () {
				$("#formlogin").on('submit', function (e) {
					e.preventDefault();
					var data = {
					    id:$("input[name=id]").val(),
                        pass:$("input[name=password]").val(),
                        remember:$("input[name=remember]:checked").val()==1?1:0
                    }
                    $.ajax({
                        url: "./ajax/checkUserLogin",
                        type: 'POST',
                        data: data,
                        dataType: 'json',
                        //contentType: 'application/json',
                        success: function(msg) {
                            if (!msg.status){
                                toastr.error("Tài khoản hoặc mật khẩu không đúng!");
                            }
                            else window.location.replace("./admin");
                        }
                    });
				})
				$("#form_register").on('submit', function (e) {
					e.preventDefault();
					var formData = new FormData($(this)[0]);
                    $.ajax({
                        url: "./ajax/saveUser",
                        type: 'POST',
                        data: formData,
						dataType: 'json',
						processData: false,
        				contentType: false,
                        //contentType: 'application/json',
                        success: function(msg) {
                            if (!msg.status){
                                toastr.error(msg.messages);
                            }
                            else window.location.replace("./admin");
                        }
                    });
				})
				$("#forgot").on('submit', function (e) {
					e.preventDefault();
					var formData = new FormData($(this)[0]);
                    $.ajax({
                        url: "./ajax/forgotPassword",
                        type: 'POST',
                        data: formData,
						dataType: 'json',
						processData: false,
        				contentType: false,
                        //contentType: 'application/json',
                        success: function(msg) {
							var formOTP = '<form id="otp" class="m-login__form m-form" action="" method="post"><div class="form-group m-form__group"><input class="form-control m-input" type="text" placeholder="Nhập mã OTP" name="otp"></div><div class="m-login__form-action"><button type="submit" class="btn m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn m-login__btn--primary font-vn">Xác thực</button></div></form>';
                            if (!msg.status){
                                toastr.error(msg.messages);
                            }
                            else {
								toastr.success(msg.messages);
								$("#forgot").replaceWith(formOTP);
							}
                        }
                    });
				})
				$("body").on('submit','#otp', function (e) {
					e.preventDefault();
					var formData = new FormData($(this)[0]);
                    $.ajax({
                        url: "./ajax/authOTP",
                        type: 'POST',
                        data: formData,
						dataType: 'json',
						processData: false,
        				contentType: false,
                        //contentType: 'application/json',
                        success: function(msg) {
							var form = '<form id="reset_pass" class="m-login__form m-form" action="" method="post"><div class="form-group m-form__group"><input class="form-control m-input" type="password" placeholder="Nhập mật khẩu mới" name="pass_new"></div><div class="m-login__form-action"><button type="submit" class="btn m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn m-login__btn--primary font-vn">Xác thực</button></div></form>';
                            if (!msg.status){
                                toastr.error(msg.messages);
                            }
                            else {
								toastr.success(msg.messages);
								$("#otp").replaceWith(form);
							}
                        }
                    });
				})
				$("body").on('submit','#reset_pass', function (e) {
					e.preventDefault();
					var formData = new FormData($(this)[0]);
                    $.ajax({
                        url: "./ajax/resetPass",
                        type: 'POST',
                        data: formData,
						dataType: 'json',
						processData: false,
        				contentType: false,
                        //contentType: 'application/json',
                        success: function(msg) {
                            if (!msg.status){
                                toastr.error(msg.messages);
                            }
                            else {
								location.reload();
							}
                        }
                    });
				})
			})
		</script>
		<script type="text/javascript">			
			if (typeof test !== 'undefined' && test[0] == 1) {				
				if (typeof test[2] !== 'undefined'){
					window.location = test[2];
				}
				else{
					toastr.success([test[1]]);
				}				
			} else if (typeof test !== 'undefined' && test[0] == 2) {				
				toastr.error(test[1]);
			}
		</script>
		<!--end::Page Snippets -->
        
        
	</body>
	<!-- end::Body -->
</html>